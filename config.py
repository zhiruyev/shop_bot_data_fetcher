from typing import Union

from pydantic_settings import (
    BaseSettings,
    SettingsConfigDict,
)
from sqlalchemy.engine.url import URL


class EnvBaseSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8", extra="ignore")


class RabbitSettings(EnvBaseSettings):
    RABBIT_USER: str
    RABBIT_PORT: int
    RABBIT_PASS: str
    RABBIT_HOST: str
    RABBIT_VHOST: str


class DBSettings(EnvBaseSettings):
    DB_HOST: str = "postgres"
    DB_PORT: int = 5432
    DB_USER: str = "postgres"
    DB_PASS: Union[str, None] = None
    DB_NAME: str = "postgres"

    @property
    def database_url(self) -> Union[URL, str]:
        if self.DB_PASS:
            return f"postgresql+asyncpg://{self.DB_USER}:{self.DB_PASS}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"
        return f"postgresql+asyncpg://{self.DB_USER}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"

    @property
    def database_url_psycopg2(self) -> str:
        if self.DB_PASS:
            return f"postgresql://{self.DB_USER}:{self.DB_PASS}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"
        return f"postgresql://{self.DB_USER}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"


class RedisSettings(EnvBaseSettings):
    REDIS_HOST: str = "redis"
    REDIS_PORT: int = 6379
    REDIS_PASS: Union[str, None] = None

    REDIS_DATABASE: int = 1

    @property
    def redis_url(self) -> str:
        if self.REDIS_PASS:
            return f'redis://{self.REDIS_PASS}@{self.REDIS_HOST}:{self.REDIS_PORT}/{self.REDIS_DATABASE}'
        return f'redis://{self.REDIS_HOST}:{self.REDIS_PORT}/{self.REDIS_DATABASE}'


class Settings(RabbitSettings, DBSettings, RedisSettings):
    DEBUG: bool = False


settings = Settings()
