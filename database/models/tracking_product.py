# ruff: noqa: TCH001, TCH003, A003, F821
from __future__ import annotations

from typing import List

from sqlalchemy import Column, BigInteger, String
from sqlalchemy.ext.asyncio import AsyncSession

from database.models.base import Base
from database.models.base_crud import gen_add, gen_getall


class TrackingProduct(Base):
    __tablename__ = "tracking_products"

    id = Column(BigInteger, primary_key=True)
    product_ref = Column(String, nullable=False, index=True)

    @classmethod
    async def getall(cls, session: AsyncSession) -> List["TrackingProduct"]:
        return await gen_getall(cls, session)

    @classmethod
    async def add(cls, obj, session: AsyncSession) -> None:
        return await gen_add(cls, obj, session)
