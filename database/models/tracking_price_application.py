# ruff: noqa: TCH001, TCH003, A003, F821
from __future__ import annotations

from sqlalchemy import Column, BigInteger, String, ForeignKey, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from database.models.base import Base
from database.models.base_crud import gen_add
from database.models.tracking_product import TrackingProduct


class TrackingPriceApplication(Base):
    __tablename__ = "tracking_price_applications"

    id = Column(BigInteger, primary_key=True)
    chat_id = Column(String, nullable=False, index=True)

    tracking_product_id = Column(BigInteger, ForeignKey(TrackingProduct.id), nullable=True)
    tracking_product = relationship("TrackingProduct")

    @classmethod
    async def add(cls, obj, session: AsyncSession) -> None:
        return await gen_add(cls, obj, session)

    @classmethod
    async def get_chat_ids_by_product(cls, product: TrackingProduct, session: AsyncSession) -> list[str]:
        result = await session.execute(
            select(cls)
            .filter(cls.tracking_product_id == product.id)
        )
        result = result.scalars().all()

        return [r.chat_id for r in result]
