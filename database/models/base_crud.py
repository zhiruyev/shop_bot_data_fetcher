from sqlalchemy import select, delete, update, func
from sqlalchemy.ext.asyncio import AsyncSession


async def gen_count(cls, session: AsyncSession, field=None, value=None):
    if value is None:
        q = select(func.count(cls.id))
        r = await session.execute(q)
        return int(r.scalar_one())
    else:
        q = select(func.count(cls.id)).where(field == value)
        r = await session.execute(q)
        return int(r.scalar_one())


async def gen_get(cls, field, value, session: AsyncSession):
    flag = False
    q = select(cls).where(field == value)
    res = await session.execute(q)
    try:
        res = res.one_or_none()
    except:
        flag = True
    if flag:
        try:
            q = select(cls).where(field == value)
            res = await session.execute(q)
            res = res.first()
        except Exception as e:
            raise Exception(e)

    if res is None:
        return None

    try:
        return res[0]
    except:
        return res


async def gen_getall(cls, field, value, session: AsyncSession):
    q = select(cls).where(field == value)
    res = await session.execute(q)
    res = res.all()
    return list(res)


async def gen_getall(cls, session: AsyncSession):
    q = select(cls)
    res = await session.execute(q)
    res = res.scalars().all()
    return list(res)


async def gen_add(cls, obj, session: AsyncSession):
    session.add(obj)
    await session.flush()


async def gen_update(cls, field, value, values: dict, session: AsyncSession):
    q = update(cls).where(field == value).values(**values)
    await session.execute(q)
    await session.commit()


async def gen_delete(cls, field, value, session: AsyncSession):
    q = delete(cls).where(field == value)
    await session.execute(q)


async def gen_pagination(cls, pagesize: int, pagenum: int, session: AsyncSession):
    q = select(cls).limit(pagesize).offset((pagenum - 1) * pagesize)
    result = await session.execute(q)
    return result.all()


async def gen_pagination_filter(cls, field, value, pagesize: int, pagenum: int, session: AsyncSession):
    q = select(cls).where(field == value).limit(pagesize).offset((pagenum - 1) * pagesize)
    result = await session.execute(q)
    return result.scalars().all()
