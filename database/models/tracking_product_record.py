# ruff: noqa: TCH001, TCH003, A003, F821
from __future__ import annotations

from sqlalchemy import Column, BigInteger, ForeignKey, Float, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from database.models.base import Base
from database.models.base_crud import gen_add
from database.models.tracking_product import TrackingProduct


class TrackingProductPriceRecord(Base):
    __tablename__ = "tracking_product_price_record"

    id = Column(BigInteger, primary_key=True)

    tracking_product_id = Column(BigInteger, ForeignKey(TrackingProduct.id), nullable=True)
    tracking_product = relationship("TrackingProduct")

    price = Column(Float, nullable=True)

    @classmethod
    async def add(cls, obj, session: AsyncSession) -> None:
        return await gen_add(cls, obj, session)

    @classmethod
    async def get_latest_by_product(cls, product: TrackingProduct, session: AsyncSession) -> TrackingProductPriceRecord:
        result = await session.execute(
            select(cls)
            .filter(cls.tracking_product_id == product.id)
            .order_by(cls.id.desc()).limit(1)
        )

        return result.scalar_one()
