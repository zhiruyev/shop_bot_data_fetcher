from sqlalchemy.exc import NoResultFound

from services.db_service import DbService
from services.parser_service import ParserService
from services.rabbit.producer import RabbitProducer
from tasks.config import broker


@broker.task(schedule=[{"cron": "*/1 * * * *"}])
async def parse_product_prices() -> None:
    products = await DbService.get_all_tracking_products()
    producer = RabbitProducer()
    for product in products:
        price = ParserService.get_product_price(product.product_ref)
        try:
            latest_record = await DbService.get_latest_tracking_product_record(product)
        except NoResultFound:
            await DbService.add_tracking_product_record(product, price)
            continue

        if latest_record.price != price:
            chat_ids = await DbService.get_chat_ids_by_product(product)
            for chat_id in chat_ids:
                producer.public_to_notify_change_price_queue({
                    'product_ref': product.product_ref,
                    'chat_id': chat_id,
                    'old_price': latest_record.price,
                    'new_price': price
                })

        await DbService.add_tracking_product_record(product, price)
