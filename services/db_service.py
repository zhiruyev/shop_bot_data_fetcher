from typing import List

from database.database import sessionmaker
from database.models import (
    TrackingProduct,
    TrackingPriceApplication,
    TrackingProductPriceRecord,
)


class DbService:

    @staticmethod
    async def add_tracking_price_application(chat_id: str, product_ref: str) -> None:
        async with sessionmaker() as session:
            async with session.begin():
                product = TrackingProduct(
                    product_ref=product_ref
                )
                await TrackingProduct.add(product, session)

                price_application = TrackingPriceApplication(
                    tracking_product=product,
                    chat_id=chat_id,
                )
                await TrackingPriceApplication.add(price_application, session)

        await session.close()

    @staticmethod
    async def get_all_tracking_products() -> List[TrackingProduct]:
        async with sessionmaker() as session:
            async with session.begin():
                tracking_products = await TrackingProduct.getall(session)
        await session.close()
        return tracking_products

    @staticmethod
    async def add_tracking_product_record(product: TrackingProduct, price: float) -> None:
        async with sessionmaker() as session:
            async with session.begin():
                price_application = TrackingProductPriceRecord(
                    tracking_product=product,
                    price=price,
                )
                await TrackingPriceApplication.add(price_application, session)

        await session.close()

    @staticmethod
    async def get_latest_tracking_product_record(product: TrackingProduct) -> TrackingProductPriceRecord:
        async with sessionmaker() as session:
            async with session.begin():
                record = await TrackingProductPriceRecord.get_latest_by_product(product, session)
        await session.close()
        return record

    @staticmethod
    async def get_chat_ids_by_product(product: TrackingProduct) -> List[str]:
        async with sessionmaker() as session:
            async with session.begin():
                chat_ids = await TrackingPriceApplication.get_chat_ids_by_product(product, session)
        await session.close()
        return chat_ids
