import logging
import time
import re

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By


logger = logging.getLogger('parser_logger')


class ParserService:

    @staticmethod
    def get_product_price(product_link: str) -> float:
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        logger.info(f'received link: {product_link}')
        driver.get(product_link)
        logger.info(f'got content for link: {product_link}')

        try:
            almaty_city = driver.find_element(By.XPATH, "//*[@data-city-id='750000000']")
            almaty_city.click()
            logger.info(f'choosed city for: {product_link}')
        except:
            pass

        time.sleep(2)
        price = driver.find_element(By.CLASS_NAME, 'item__price-once').get_attribute('innerText')
        logger.info(f'parsed price: {product_link}')

        price = re.sub('\D', '', price)

        logger.info(f'price for {product_link} is {price}')
        driver.quit()

        return float(price)
