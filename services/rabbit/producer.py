import json

from config import settings
from .base.basic_rabbit_client import RabbitClient


class RabbitProducer(RabbitClient):
    def __init__(self):
        super().__init__(
            settings.RABBIT_HOST,
            settings.RABBIT_PORT,
            settings.RABBIT_USER,
            settings.RABBIT_PASS,
            settings.RABBIT_VHOST,
        )
        self.__notify_change_price_channel = None

        self.__notify_change_price_exchange_name = 'notify_change_price_direct'
        self.__notify_change_price_queue_name = 'notify_change_price'

    def __declare_notify_change_price_queue(self):
        self.__notify_change_price_channel = self._connection.channel()
        self.__notify_change_price_channel.exchange_declare(
            exchange=self.__notify_change_price_exchange_name,
            exchange_type='direct',
            durable=True,
        )
        self.__notify_change_price_channel.queue_declare(queue=self.__notify_change_price_queue_name, durable=True)

    def public_to_notify_change_price_queue(self, body: dict):
        self._create_connection()
        self.__declare_notify_change_price_queue()
        self.__notify_change_price_channel.basic_publish(
            exchange=self.__notify_change_price_exchange_name,
            routing_key=self.__notify_change_price_queue_name,
            body=json.dumps(body),
        )
        self._connection.close()
