import json

from .basic_rabbit_client import RabbitClient


class BasicConsumer(RabbitClient):
    def __init__(
        self,
        host: str,
        port: int,
        username: str,
        password: str,
        virtual_host: str,
        exchange_name: str,
        queue_name: str,
    ):
        super().__init__(host, port, username, password, virtual_host)
        self._connection = None
        self.reg_apps_channel = None

        self.exchange_name = exchange_name
        self.queue_name = queue_name

    def __create_channel(self):
        self._channel = self._connection.channel()
        self._channel.exchange_declare(exchange=self.exchange_name, exchange_type='direct', durable=True)
        self._channel.queue_declare(queue=self.queue_name, durable=True)
        self._channel.queue_bind(exchange=self.exchange_name, queue=self.queue_name)

    @staticmethod
    def _callback_func(data):
        raise NotImplementedError()

    def callback(self, channel, method, properties, body):
        data = json.loads(body)
        self._callback_func(data)

    def consume(self):
        self._create_connection()
        self.__create_channel()

        self._channel.basic_consume(
            queue=self.queue_name,
            on_message_callback=self.callback,
        )

        self._channel.start_consuming()
