from abc import ABC

import pika


class RabbitClient(ABC):
    def __init__(self, host: str, port: int, username: str, password: str, virtual_host: str):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._virtual_host = virtual_host

    def _create_connection(self):
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self._host,
                port=self._port,
                virtual_host=self._virtual_host,
                credentials=pika.PlainCredentials(
                    username=self._username,
                    password=self._password,
                )
            )
        )
